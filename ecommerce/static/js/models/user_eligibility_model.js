define([
        'backbone'
    ],
    function (Backbone) {
        'use strict';

        // Stores our user eligibility information

        return Backbone.Model.extend({});

    }
);
