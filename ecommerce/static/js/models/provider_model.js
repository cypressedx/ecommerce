define([
        'backbone'
    ],
    function (Backbone) {
        'use strict';

        // Stores our provider information

        return Backbone.Model.extend({});

    }
);

